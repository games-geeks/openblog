# Sommaire
- [Sommaire](#sommaire)
- [Le projet](#le-projet)
  - [Technologies](#technologies)
  - [Base de données](#base-de-données)
  - [Vidéos](#vidéos)
  - [Maquettes](#maquettes)
    - [Page d'accueil](#page-daccueil)
      - [Mobile](#mobile)
      - [Desktop](#desktop)
    - [Page d'un article](#page-dun-article)
      - [Mobile](#mobile-1)
      - [Desktop](#desktop-1)

# Le projet

Open**Blog** est un **blog multi auteurs** créé avec **Symfony 7** dans une série de tutoriels présents sur la chaîne **Nouvelle-Techno.fr** à cette adresse : https://www.youtube.com/playlist?list=PLBq3aRiVuwywmwPHz0BzPFvH0P-37mH8K

## Technologies

Open**Blog** sera développé en utilisant :

- Docker
- Symfony 7
- Mysql 8.0
- PHP 8.2
- PHPMyAdmin
- Sass

L'utilisation de bundles sera limitée au strict nécessaire.

## Base de données
Vous trouverez le **schéma de base de données** ici : https://drawsql.app/teams/ma-team-7/diagrams/blog-symfony-7

![Database](assets/images/project/Database.png)

## Vidéos

1. [Présentation et configuration du projet](#) ([Commit Github](#))
2. A venir

## Maquettes

Les maquettes sont disponibles sur [Figma](https://www.figma.com/file/WBF5w0A2qQ6qCfcMPP4Per/OpenBlog?type=design&node-id=0%3A1&mode=design&t=Fm5lnbz8ojK7uSlb-1)

### Page d'accueil

#### Mobile

![Page d'accueil Mobile](assets/images/project/HomePageMobile.png)

#### Desktop

![Page d'accueil Desktop](assets/images/project/HomePage.png)

### Page d'un article

#### Mobile

![Page d'un article Mobile](assets/images/project/SinglepostMobile.png)

#### Desktop

![Page d'un article Desktop](assets/images/project/Singlepost.png)